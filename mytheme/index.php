<?php
get_header();
?>

<section class="main">
    <div class="container">

        <main id="primary" class="content-area">
            <!-- the loop -->
            <?php if (have_posts()) : ?>
                <?php while (have_posts()) : the_post(); ?>
                    <div class="post">
                        <h1><a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a></h1>
                        <?php the_time('l, F jS, Y') ?>
                        <div class="entry">
                            <?php the_post_thumbnail(); ?>
                            <?php the_content(); ?>
                            <p class="postmetadata">
                                <?php _e('Filed under&#58;'); ?>
                                <?php the_category(', ') ?>
                                <?php _e('by'); ?>
                                <?php the_author(); ?>
                                <br />
                                <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
                                <?php edit_post_link('Edit', ' &#124; ', ''); ?>
                            </p>
                            <br />
                        </div>
                    </div>
                <?php endwhile; ?>
                <div class="navigation">
                    <?php posts_nav_link(); ?>
                </div>
            <?php endif; ?>
            <!-- einde the loop -->
        </main>

        <!-- print sidebar -->
        <?php if (is_active_sidebar('custom-side-bar')) : ?>
            <?php dynamic_sidebar('custom-side-bar'); ?>
        <?php endif; ?>
    </div>
</section>


<footer id="colophon" class="site-footer">
    <div class="container">
        <?php wp_footer(); ?>
    </div>
</footer>