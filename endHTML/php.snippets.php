<!-- the loop -->
<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <div class="post">
            <h1><a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                </a></h1>
            <?php the_time('l, F jS, Y') ?>
            <div class="entry">
                <?php the_post_thumbnail(); ?>
                <?php the_content(); ?>
                <p class="postmetadata">
                    <?php _e('Filed under&#58;'); ?>
                    <?php the_category(', ') ?>
                    <?php _e('by'); ?>
                    <?php the_author(); ?>
                    <br />
                    <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
                    <?php edit_post_link('Edit', ' &#124; ', ''); ?>
                </p>
                <br />
            </div>
        </div>
    <?php endwhile; ?>
    <div class="navigation">
        <?php posts_nav_link(); ?>
    </div>
<?php endif; ?>
<!-- einde the loop -->


<!-- print sidebar -->
<?php if (is_active_sidebar('custom-side-bar')) : ?>
    <?php dynamic_sidebar('custom-side-bar'); ?>
<?php endif; ?>


<!-- print footer -->
<?php wp_footer(); ?>










<!-- functions.php -->
<?php

// Add Style.css
// Read documentation: https://developer.wordpress.org/themes/basics/including-css-javascript/
wp_enqueue_style('style', get_stylesheet_uri());

// Add your own jQuery:
wp_enqueue_script('my-custom-script', get_template_directory_uri() . '/js/script.js', array('jquery'), null, true);


// Add Sidebar
function my_custom_sidebar()
{
    register_sidebar(
        array(
            'name' => __('Custom', 'your-theme-domain'),
            'id' => 'custom-side-bar',
            'description' => __('Custom Sidebar', 'your-theme-domain'),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
}
add_action('widgets_init', 'my_custom_sidebar');
